export class ZonaOperativa {
    name: string = null;
    identification: string = null;
    type: string = null;
    subtype: string = null;
    characteristics: any = null;
    fathers: any = null;
}