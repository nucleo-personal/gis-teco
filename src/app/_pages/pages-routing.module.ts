import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';
import { 
  AuthGuardService as AuthGuard 
} from 'src/services/auth-guard';
import { ZonasOperativasComponent } from './zonas-operativas/zonas-operativas.component';
import { DepartamentosComponent } from './departamentos/departamentos.component';
import { CiudadesComponent } from './ciudades/ciudades.component';
import { CallesComponent } from './calles/calles.component';


const routes: Routes = [
  {
    path: "",
    component: PagesComponent,
    children: [
      {
        path: "",
        redirectTo: "zonas-operativas",
        pathMatch: "full"
      },
      {
        path: "zonas-operativas",
        component: ZonasOperativasComponent,
        data: {index:0}
      },
      {
        path: "departamentos",
        component: DepartamentosComponent,
        data: {index:1}
      },
      {
        path: "ciudades",
        component: CiudadesComponent,
        data: {index:2}
      },
      {
        path: "calles",
        component: CallesComponent,
        data: {index:3}
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
