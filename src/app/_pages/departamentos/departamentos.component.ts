import { Component, OnInit } from '@angular/core';
import { Departamento } from 'src/models/departamento';
import { DepartamentoAPI } from 'src/services/departamento.service';

@Component({
  selector: 'app-departamentos',
  templateUrl: './departamentos.component.html',
  styleUrls: ['./departamentos.component.scss']
})
export class DepartamentosComponent implements OnInit {

  constructor(private apiDep: DepartamentoAPI){}

  ngOnInit(): void {
    this.getDepartamentos();
  }

  departamentos: Departamento[] = [];
  p: number = 1;
  headers = [
    {nombre: "Departamento", w: 45},
    {nombre: "País", w: 45}, 
  ];

  getDepartamentos = () => {
    this.apiDep.get().subscribe(
      data => {
        this.departamentos = data;
        console.log(data);
      }
    )
  }

}
