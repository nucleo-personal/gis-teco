import { Component, OnInit } from '@angular/core';
import { Ciudad } from 'src/models/ciudad';
import { Departamento } from 'src/models/departamento';
import { CiudadAPI } from 'src/services/ciudad.service';
import { DepartamentoAPI } from 'src/services/departamento.service';

@Component({
  selector: 'app-ciudades',
  templateUrl: './ciudades.component.html',
  styleUrls: ['./ciudades.component.scss']
})
export class CiudadesComponent implements OnInit {

  constructor(private apiCiudad: CiudadAPI, private apiDep: DepartamentoAPI) { }

  ciudades: Ciudad[] = [];
  departamentos: Departamento[] = [];
  departamentoTarget = null;
  p: number = 1;
  headers = [
    {nombre: "Ciudad", w: 45},
    {nombre: "Departamento", w: 45}, 
  ];

  ngOnInit(): void {
    this.getCiudades();
  }

  getCiudades = async() => {
    await this.apiDep.get().subscribe(
      data => {
        this.departamentos = data;
        this.departamentoTarget = this.departamentos[0].identification;
        console.log(this.departamentos);
      }
    );

    await this.apiCiudad.get().subscribe(
      data => {
        this.ciudades = data;
        console.log(this.ciudades);
      }
    )
  }
}
