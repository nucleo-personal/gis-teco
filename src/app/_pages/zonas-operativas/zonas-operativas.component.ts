import { Component, OnInit } from '@angular/core';
import { ZonaOperativa } from 'src/models/zona-operativa.model';
import { ZonaOperativaAPI } from 'src/services/zona-operativa.service';

@Component({
  selector: 'app-zonas-operativas',
  templateUrl: './zonas-operativas.component.html',
  styleUrls: ['./zonas-operativas.component.scss']
})
export class ZonasOperativasComponent implements OnInit {

  constructor(private apiZona: ZonaOperativaAPI) { }

  ngOnInit(): void {
    this.getZonas();
  }

  zonasOperativas: ZonaOperativa[] = [];
  p: number = 1;
  headers = [
    {nombre: "Nombre", w: 30},
    {nombre: "Identificación", w: 60}, 
  ];

  getZonas = () => {
    this.apiZona.get().subscribe(
      data => {
        this.zonasOperativas = data;
        console.log(data);
      }
    )
  }

}
