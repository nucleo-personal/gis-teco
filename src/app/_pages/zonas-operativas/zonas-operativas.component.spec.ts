import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ZonasOperativasComponent } from './zonas-operativas.component';

describe('ZonasOperativasComponent', () => {
  let component: ZonasOperativasComponent;
  let fixture: ComponentFixture<ZonasOperativasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ZonasOperativasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ZonasOperativasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
