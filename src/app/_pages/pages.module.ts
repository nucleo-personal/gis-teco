import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesComponent } from './pages.component';
import { PagesRoutingModule } from './pages-routing.module';
import { ZonasOperativasComponent } from './zonas-operativas/zonas-operativas.component';
import { DepartamentosComponent } from './departamentos/departamentos.component';
import { CiudadesComponent } from './ciudades/ciudades.component';
import { CallesComponent } from './calles/calles.component';
import { HttpClientModule } from '@angular/common/http';
import {NgxPaginationModule} from 'ngx-pagination';
import { NgSelectModule } from '@ng-select/ng-select';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [PagesComponent, ZonasOperativasComponent, DepartamentosComponent, CiudadesComponent, CallesComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    NgxPaginationModule,
    NgSelectModule, 
    FormsModule,
    PagesRoutingModule
  ]
})
export class PagesModule { }
