import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

declare var $:any;
@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {

  active;
  constructor(private route: ActivatedRoute) { 
    this.active = route.snapshot.firstChild.data;
  }

  ngOnInit(): void {
    $("#sidebarToggle").on("click", function(e) {
      e.preventDefault();
      $("body").toggleClass("sb-sidenav-toggled");
    });
  }

  items = [
    {
      nombre: "Zonas Operativas",
      route: "/zonas-operativas",
      icon: "fas fa-dot-circle"
    },
    {
      nombre: "Departamentos",
      route: "/departamentos",
      icon: "fas fa-dot-circle"
    },
    {
      nombre: "Ciudades",
      route: "/ciudades",
      icon: "fas fa-dot-circle"
    },
    {
      nombre: "Calles",
      route: "/calles",
      icon: "fas fa-dot-circle"
    }
  ]

  changeRoute(index){
    this.active['index'] = index;
  }
}
