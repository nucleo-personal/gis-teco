import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import { Host } from './host';

@Injectable({
  providedIn: 'root'
})
export class CiudadAPI {

  constructor(private http: HttpClient) { }

  get():Observable<any>{
    let httpParams = new HttpParams();
    // httpParams = httpParams.append("by", "fecha_modificacion");
    // httpParams = httpParams.append("order", "DESC");  
    return this.http.get(`../assets/json/ciudad.json`, {params: httpParams});
  }
  
}